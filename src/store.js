import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({

    state: {
        status: '',
        message: '',
        token: localStorage.getItem('token') || '',
        user : ''
    },
  
    // getters
    getters : {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
    },
    // actions
    actions: {
        login({commit}, user){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({url: 'http://localhost:8000/api/login', data: user, method: 'POST' })
                .then(resp => {
                    console.log(resp)
                    const data = resp.data
                    localStorage.setItem('token', resp.data.access_token)
                    // Add the following line:
                    //axios.defaults.headers.common['Authorization'] = token
                    
                    commit('auth_success', data)
                    resolve(resp)
                })
                .catch(err => {
                    commit('auth_error', err)
                    localStorage.removeItem('token')
                    reject(err)
                })
            })
        },

        /*register({commit}, user){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({url: 'http://localhost:3000/register', data: user, method: 'POST' })
                .then(resp => {
                    const token = resp.data.token
                    const user = resp.data.user
                    localStorage.setItem('token', token)
                    // Add the following line:
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', token, user)
                    resolve(resp)
                })
                .catch(err => {
                    commit('auth_error', err)
                    localStorage.removeItem('token')
                    reject(err)
                })
            })
        },*/

        logout({commit}){
            return new Promise((resolve, reject) => {
                commit('logout')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        }

    },
  
    // mutations
    mutations: {
        auth_request(state){
            state.status = 'Carregando'
        },

        auth_success(state, data){
            state.status = data.statusText
            state.token = data.access_token
            state.user = data.user
            state.message = data.message
        },

        auth_error(state, err){
            state.status = 'Erro'
            state.message = err
        },
        
        logout(state){
            state.status = ''
            state.token = ''
        },
    }

})