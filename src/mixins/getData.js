import axios from '../http'
import { mapState } from 'vuex'

const getData = {
    props:['mode', 'resource'],
    data() {
        return {
            rows: [],
        }
    },
    methods: {
        fetchData() {
            if(this.mode == 'list') {
                axios.get(`http://localhost:8000/api/`+this.resource,  { 
                    headers: {
                      'Authorization': `Bearer ${this.token}`
                    }
                  })
                .then(response => {
                    this.rows = response.data.rows
                        for(let i in this.rows){
                            this.rows[i].Ações = {
                               id: this.rows[i].id, 
                                id: this.rows[i].id, 
                               id: this.rows[i].id, 
                            }
                        }
                })
                .catch(e => {
                    this.errors.push(e)
                })
            }

            if(this.mode == 'show') {
                axios.get(`http://localhost:8000/api/`+this.resource+'/'+this.$route.params.id,  {
                    headers: {
                      'Authorization': `Bearer ${this.token}`
                    }
                  })
                .then(response => {
                    this.fields = response.data.item
                })
                .catch(e => {
                    this.errors.push(e)
                })
            }
            
            /*if(this.mode == 'create') {
                this.breadcrumb = response.data.breadcrumb
                this.fields = response.data.fields
                this.properties = response.data.properties
                this.mode = response.data.mode
            } 

            if (this.mode == 'edit') {
                this.breadcrumb = response.data.breadcrumb
                this.fields = response.data.fields
                this.properties = response.data.properties
                this.mode = response.data.mode
            }*/ 
        },
    },
    mounted() {
        this.fetchData();
    },
    computed: {
        ...mapState(['status','token','user'])       
    }

}

export default getData