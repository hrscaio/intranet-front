import axios from '../http'
import { mapState } from 'vuex'

const sendData = {
    props:['mode', 'resource', 'id'],
    data() {
        return {
          error: null,
          alert: null,
        }
    },
    methods: {

        onSubmit() {
            if(this.mode == 'store') {
                axios.post(`http://localhost:8000/api/`+this.resource, {body: this.fields}, { headers: { 'Authorization': `Bearer ${this.token}`}})
                .then(response => { 
                    this.alert = response.data.sucess
                    this.showErrors(response.data.errors)
                })
                .catch(e => {})
            }

            if(this.mode == 'show') {
                axios.put(`http://localhost:8000/api/`+this.resource+'/'+this.id, {body: this.fields}, { headers: { 'Authorization': `Bearer ${this.token}`}})
                .then(response => { 
                    this.alert = response.data.sucess 
                    this.showErrors(response.data.errors)
                })
            }
        },

        showErrors(errors) {    
            for(let i in this.properties) {                 
                this.properties[i].erro =  '';
                this.properties[i].hasError =  false
            }  

            for (var column in errors) { 
                for(let i in this.properties) {                    
                    if (column == this.properties[i].name) {
                        this.properties[i].erro =  errors[column];
                        this.properties[i].hasError =  true
                    } 
                }  
            }
        },

        
        deleteItem(id) {
            if(confirm("Tem certeza que deseja deletar este registro?")){
                axios.post( `http://localhost:8000/api`+this.$route.fullPath+'/'+id, {_method: 'delete'},  { headers: { 'Authorization': `Bearer ${this.token}`}})
                .then(
                    response => {    
                        this.alert = response.data.sucess
                        this.$emit('fetchData');
                    }
                )
                .catch(
                    e => {this.errors.push(e)}
                )
            }
        },
    },
    computed: {
        ...mapState(['status','token','user'])       
    }
}

export default sendData