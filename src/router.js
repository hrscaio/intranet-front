import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Login from './views/Login.vue'
import Register from './components/Register.vue'
import CompanyList from './views/settings/company/CompanyList.vue'
import CompanyForm from './views/settings/company/CompanyForm.vue'
import DepartmentList from './views/settings/department/DepartmentList.vue'
import DepartmentForm from './views/settings/department/DepartmentForm.vue'
import UserList from './views/settings/users/UserList.vue'
import UserForm from './views/settings/users/UserForm.vue'

Vue.use(Router)

let router = new Router({
	mode: 'history',
  	routes: [
    	{ path: '/', name: 'login', component: Login},
    	{ path: '/teste', name: 'register',component: Register},
		
		{ path: '/company', name: 'Lista de Empresas', component: CompanyList, meta: { requiresAuth: true}, props: { mode: 'list', resource: 'company' }},
		{ path: '/company/store/', name: 'Cadastro de Empresa', component: CompanyForm, meta: {  requiresAuth: true },  props: { mode: 'store', resource: 'company' }},
		{ path: '/company/:id', name: 'Editar Empresa', component: CompanyForm, meta: {  requiresAuth: true }, props: { mode: 'show', resource: 'company' }},

		{ path: '/department', name: 'Lista de Departamentos', component: DepartmentList, meta: { requiresAuth: true}},
		{ path: '/department/store', name: 'Cadastro de Departamento', component: DepartmentForm, meta: {  requiresAuth: true }},
		{ path: '/department/:id/', name: 'Editar Departamento', component: DepartmentForm, meta: {  requiresAuth: true }},

		{ path: '/register', name: 'Lista de Usuários', component: UserList, meta: { requiresAuth: true}},
		{ path: '/register/create', name: 'Cadastro de Usuário', component: UserForm, meta: {  requiresAuth: true }},
		{ path: '/register/:id/edit', name: 'Editar Usuário', component: UserForm, meta: {  requiresAuth: true }},
  	]
})

router.beforeEach((to, from, next) => {
	if(to.matched.some(record => record.meta.requiresAuth)) {
    	if(store.getters.isLoggedIn) {
      		next()
      		return
    	}
    	next('/') 
  	} else {
    	next() 
  	}
})

export default router