import axios from 'axios'

const state = () => ({
    rows: '',
    status: ''
})
  
// getters
const getters = {
    teste: state => !!state.rows,
    teste1: state => state.status,
}
  
// actions
const actions = {

    get({commit}, company){

        return new Promise((resolve, reject) => {
            commit('request_company')
            axios.get(`http://localhost:8000/api/company`,  { headers: {'Authorization': `Bearer ${this.token}`}})
            .then(response => {
                company = response.data.rows
                commit('auth_success', company)
                resolve(response)
            })
            .catch(e => {
                commit('request_error')
                reject(err)
            })
        })
    },
}
  
// mutations
const mutations = {
    request_company(state){
        state.status = 'loading'
    },

    company_success(state, rows){
        state.status = 'success'
        state.rows = rows
    },

    request_error(state){
        state.status = 'error'
    },

}
  
export default {
    //namespaced: true,
    state,
    getters,
    actions,
    mutations
}